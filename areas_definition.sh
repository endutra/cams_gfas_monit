#!/bin/bash

## script to set up environment variables used by the scripts
## E. Dutra, IPMA April 2024 

set -eua

## Regions definitions
DOMAINS=('Globe'\
         'Africa'\
         'Asia'\
         'Australia'\
         'Europe'\
         'Indonesia'\
         'North_America'\
         'South_America'\
         'Amazon'\
         )
DOMAINSL=('-180,180,-90,90'\
          '-30,60,-35,35'\
          '60,180,-10,66'\
          '100,180,-50,-10'\
          '-30,60,35,66'\
          '90,170,-10,5'\
          '-170,-50,15,66'\
          '-95,-30,-60,15'\
          '-95,-30,-20,0'\
          )
# ${#DOMAINS[@]} 
set +eua
