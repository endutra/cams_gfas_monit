#!/bin/bash 

## script process GFAS monitoring daily 
## E. Dutra, IPMA April 2024 

module load ecmwf-toolbox
module load python3 

set -eu

print_help() {
	echo "Usage: "process_gfas_monit_daily.sh" <args>" >&2
	exit 0
}
opts=$(getopt -o '' --long class:,expver:,edate:,efreq:,draw:,dstats:,dplot:,dformat:,gsdvars: -- "$@") || print_help
eval set -- "$opts"
while (($#))
do
  case $1 in
    --class)   class=$2; shift;;
    --expver)  expver=$2; shift;;
    --edate)   edate=$2; shift;;
    --efreq)   efreq=$2; shift;;
    --draw)    draw=$2; shift;;
    --dstats)  dstats=$2; shift;;
    --dplot)   dplot=$2; shift;;
    --dformat) dformat=$2; shift;;
    --gsdvars)  gsdvars=$2; shift;;
    --)           shift; break;;
    esac
    shift 
done

# set -x
 
start=$(date +'%s') 
echo "Starting process_gfas_monit_daily.sh $class $expver $edate $efreq $draw $dstats $dplot $dformat"


## Extract and compute stats 

## gridded analysis
./extract_mars_ga.sh --class $class --expver $expver --efreq $efreq \
                     --edate $edate --draw $draw --dstats $dstats
./compute_stats.sh --class $class --expver $expver --efreq $efreq \
                   --edate $edate --draw $draw --dstats $dstats --dtype ga

## gridded satellite data 
./extract_mars_gsd.sh --class $class --expver $expver --efreq $efreq \
                      --edate $edate --draw $draw --dstats $dstats --gsdvars $gsdvars
./compute_stats.sh --class $class --expver $expver --efreq $efreq \
                      --edate $edate --draw $draw --dstats $dstats --dtype gsd
                      

## Do the python plots 
if [ $dformat == "NONE" ]; then
    echo "Skipping ploting"
    exit 0 
fi 

mkdir -p $dplot


## Get region definitions 
. ./areas_definition.sh 

## Loop in domains 
ndomains=${#DOMAINS[@]} 
for (( i=0; i<$ndomains; i++));
do
    echo "Processing ${DOMAINS[i]} ${DOMAINSL[i]}"

    for ptype in GA GSD GSD_C
    do

        python3 -u ./plot_stats.py --ptype $ptype \
                                   --dstats $dstats \
                                   --domain ${DOMAINS[i]} \
                                   --expver $expver \
                                   --class $class \
                                   --edate $edate \
                                   --efreq $efreq \
                                   --dplot $dplot \
                                   --dformat $dformat 
    done # loop on plot types
done # loop on domains 


                      
echo "Done process_gfas_monit_daily.sh $class $expver $edate in  $(($(date +'%s') - $start)) seconds"
