#!/bin/bash 

## script to extract GFAS GSD satellite observations
## E. Dutra, IPMA April 2024 


module load ecmwf-toolbox
module load cdo
module load nco

set -eu

print_help() {
	echo "Usage: "extract_mars_ga.sh" <args>" >&2
	exit 0
}
opts=$(getopt -o '' --long class:,expver:,edate:,efreq:,draw:,dstats:,gsdvars: -- "$@") || print_help
eval set -- "$opts"
while (($#))
do
  case $1 in
    --class)   class=$2; shift;;
    --expver)  expver=$2; shift;;
    --edate)   edate=$2; shift;;
    --efreq)   efreq=$2; shift;;
    --draw)    draw=$2; shift;;
    --dstats)  dstats=$2; shift;;
    --gsdvars)  gsdvars=$2; shift;;
    --)        shift; break;;
    esac
    shift 
done

# set -x

start=$(date +'%s') 
echo "Starting extract_mars_gsd.sh $class $expver $edate $efreq $draw $dstats"


# Create draw output directory 
mkdir -p $draw
mkdir -p $dstats

## daily mean if efreq== 1d
cdodaymean=""
if [ $efreq == "1d" ]; then
    cdodaymean=" -daymean " 
fi 

## case of monthly retrieval
if [ ${#edate} -eq 6 ] ; then
    ldate=$(date -u -d "${edate}01 +1 month -1 day" +"%Y%m%d")
    ddate="${edate}01/to/${ldate}"
else
    ddate=$edate
fi
    
ftag=${class}_${expver}_${edate}_${efreq} # file name "tag"
fout=${draw}/gfas_gsd_${ftag}  # output fil


set +e ## continue ... 

## for each GSD type if in gsdvars, extract the data ... 
cvars=""
if [[ $gsdvars == @(*MODISTerra*) ]]; then 
    echo "MODISTerra";
    cvars="$cvars 783_389"
mars << EOF
retrieve,
class=$class,
date=$ddate,
expver=$expver,
ident=783,
instrument=389,
param=97.210/99.210,
stream=gfas,
time=0/to/23/by/1,
type=gsd,
target="${fout}_[ident]_[instrument].grb"
EOF
fi 
if [[ $gsdvars == @(*MODISAqua*) ]]; then
    echo "MODISAqua";
    cvars="$cvars 784_389"
mars << EOF
retrieve,
class=$class,
date=$ddate,
expver=$expver,
ident=784,
instrument=389,
param=97.210/99.210,
stream=gfas,
time=0/to/23/by/1,
type=gsd,
target="${fout}_[ident]_[instrument].grb"
EOF
fi 
if [[ $gsdvars == @(*VIIRSNPP*) ]]; then
    echo "VIIRSNPP";
    cvars="$cvars 224_616"
mars << EOF
retrieve,
class=$class,
date=$ddate,
expver=$expver,
ident=224,
instrument=616,
param=97.210/99.210,
stream=gfas,
time=0/to/23/by/1,
type=gsd,
target="${fout}_[ident]_[instrument].grb"
EOF
fi 

## Loop on the ident_intrument tags 
ffs=""
for ff in $cvars
do
    fname=${fout}_${ff}
    if [ -r ${fname}.grb ] ; then 
        ## interpolate to regular 1x1 conservative and netcdf 
        echo "Processing: ${fname}.grb"
        if [ ! -r ${dstats}/remapw.nc ]; then
            cdo --eccodes -P 2 -f nc4 -z zip_1 gencon,global_1 ${fname}.grb ${dstats}/remapw.nc
        fi 
        cdo --eccodes -f nc4 -z zip_1 remap,global_1,${dstats}/remapw.nc ${cdodaymean} ${fname}.grb ${fname}.nc 2>/dev/null
        rm -f ${fname}.grb
    
        # rename vars adding ident_intrument to variable name (to allow latter merge)
        ncrename -O -v offire,offire_${ff} -v frpfire,frpfire_${ff} ${fname}.nc ${fname}.nc
        ffs="${ffs} ${fname}.nc"
    fi 
done

## merge in a single file
cdo -O -f nc4 -z zip_1 merge $ffs ${fout}.nc 
rm -f ${ffs}
set -e
echo "Done extract_margs_gsd ${ftag} in  $(($(date +'%s') - $start)) seconds"
