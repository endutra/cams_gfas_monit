# cams_gfas_monit

## Description
This repository contains the scripts to monitor CAMS GFAS 


## Installation
Requires ECMWF environment. Tested in ecs-login 

Uses: /bin/bash and python3 packages: xarrar, numpy, matplotlib, pandas

## contents
- areas_definition.sh  - definition of areas to compute statistics 
- extract_mars_ga.sh   - extract GFAS gridded assimilated FRP 
- extract_mars_gsd.sh  - extract GFAS gridded satellite data FRP 
- compute_stats.sh     - compute area statistics
- generate_stats_historical.sh  - extract and generate statistics for an historical period
- process_gfas_monit_daily.sh  - extract, generate statistics and produce plots 
- plot_stats.py      - produce plots 

## Usage

```sh 
git clone git@gitlab.com:endutra/cams_gfas_monit.git
cd cams_gfas_monit 
./{script}.sh <options>
```

### Daily run 

```sh 
# script will extract the date, compute the regional statistics (appending to existing) and to the plots
./process_gfas_monit_daily.sh --class $class # data class : mc / rd  
                              --expver $expver # experiment version 0001 / 0010 
                              --edate ${YYYYMMDD}  # date YYYMMDD to process 
                              --efreq "1d"        # frequency to extract data: 1d -> daily ; 1h -> hourly 
                              --draw  ./        # to store raw data, temporary, e.g $SCRATCH/cams_monit/RAW1 
                              --dstats $DSTATS  # to store statistics - permanent, e.g. $PERM/cams_monit1/ 
                              --dplot  $DPLOT   #to store plot , e.g. $PERM/cams_monit1_plot 
                              --gsdvars "MODISTerra_MODISAqua_VIIRSNPP" #GSD data to extract and process any of combinations possible
                                                                        # e.g MODISTerra , MODISAqua_VIIRSNPP , etc... 
                              --dformat png    # output figure format: png / pdf / NONE - skip producing plots. 

```


### Generate historical data 
This step only has to be done once for each experiment. 
```sh 
# run (under ecinteractive, or submit a job as this can take a while to run... )

## example for mc expver 0001 
./generate_stats_historical.sh    --class mc --expver 0001 --efreq 1d --draw $SCRATCH/cams_monit/RAW1 \
                                  --dstats $PERM/cams_monit1/ --gsdvars "MODISTerra_MODISAqua" \
                                  --START_MON 201901 --END_MON 202404 --START_DAY 20240401 --END_DAY 20240415
                                  
## example for mc expver 0010 
./generate_stats_historical.sh    --class mc --expver 0010 --efreq 1d --draw $SCRATCH/cams_monit/RAW1 \
                                  --dstats $PERM/cams_monit1/ --gsdvars "MODISTerra_MODISAqua" \
                                  --START_MON 201901 --END_MON 202404 --START_DAY 20240401 --END_DAY 20240415
                                  
## example for rd i07p
./generate_stats_historical.sh    --class rd --expver i07p --efreq 1d --draw $SCRATCH/cams_monit/RAW1 \
                                  --dstats $PERM/cams_monit1/ --gsdvars "MODISTerra_MODISAqua_VIIRSNPP" \
                                  --START_MON 202306 --END_MON 202306 --START_DAY 1 --END_DAY 0

```

### Daily catch-up after historical
In case it is necessary to catch-up the statistics from the historical to a current date
```sh 

## example for mc expver 0010  to extract day 16 to 30 of April 2024 
./generate_stats_historical.sh    --class mc --expver 0010 --efreq 1d --draw $SCRATCH/cams_monit/RAW1 \
                                  --dstats $PERM/cams_monit1/ --gsdvars "MODISTerra_MODISAqua" \
                                  --START_MON 1 --END_MON 0 --START_DAY 20240416 --END_DAY 20240430

```

### Manual plotting 
Produce single plots 
```sh 
## example for rd i07p globe 
module load ecmwf-toolbox
module load python3 
for ptype in GA GSD GSD_C
do
    python3 -u ./plot_stats.py --class rd --expver i07p --edate 20230630 --efreq "1d" \
                                --dstats $PERM/cams_monit1/ --dplot  $PERM/cams_monit1_plot/ \
                                --dformat pdf  --domain Globe --ptype $ptype
done
```

## Authors and acknowledgment
Emanuel Dutra, 2024 , IPMA. Developed under the project:  CAMS2_64_KCL - Development of the Global Fire Assimilation System (GFAS)

