#!/bin/bash 

## script to extract and generate historical statistics 
## E. Dutra, IPMA April 2024 


set -eux 

print_help() {
	echo "Usage: "generate_stats_historical.sh" <args>" >&2
	exit 0
}
opts=$(getopt -o '' --long class:,expver:,edate:,efreq:,draw:,dstats:,dplot:,dformat:,gsdvars:,START_MON:,END_MON:,START_DAY:,END_DAY: -- "$@") || print_help
eval set -- "$opts"
while (($#))
do
  case $1 in
    --class)   class=$2; shift;;
    --expver)  expver=$2; shift;;
    --efreq)   efreq=$2; shift;;
    --draw)    draw=$2; shift;;
    --dstats)  dstats=$2; shift;;
    --gsdvars)  gsdvars=$2; shift;;
    --START_MON) START_MON=$2; shift;;
    --END_MON)   END_MON=$2; shift;;
    --START_DAY) START_DAY=$2; shift;;
    --END_DAY)   END_DAY=$2; shift;;    
    --)           shift; break;;
    esac
    shift 
done


# ## MC class example 
#class="mc"
#expver="0010"
#efreq="1d"
#gsdvars="MODISTerra_MODISAqua"
#draw="$SCRATCH/cams_monit/RAW1"
#dstats="$PERM/cams_monit1/"
 
#START_MON=201901  # start of monthly chunks extration
#END_MON=202403  # end of monthly chunks extraction
#START_DAY=20240401 # first day of daily extraction
#END_DAY=20240430  # last day of daily extraction,

#START_MON=202401  # start of monthly chunks extration
#END_MON=202404  # end of monthly chunks extraction
#START_DAY=20240401 # first day of daily extraction
#END_DAY=20240400  # last day of daily extraction,

### RD class example  1 
#class="rd"
#expver="i1j7"
#efreq="1d"
#draw="$SCRATCH/cams_monit/RAW1"
#dstats="$PERM/cams_monit1/"

#START_MON=201901  # start of monthly chunks extration
#END_MON=202305  # end of monthly chunks extraction
#START_DAY=0     # first day of daily extraction  
#END_DAY=0       # last day of daily extraction

# ### RD class example  2 
# class="rd"
# expver="i07p"
# efreq="1d"
# draw="$SCRATCH/cams_monit/RAW1"
# dstats="$PERM/cams_monit1/"
# 
# START_MON=202207  # start of monthly chunks extration
# END_MON=202306  # end of monthly chunks extraction
# START_DAY=0     # first day of daily extraction  
# END_DAY=0       # last day of daily extraction


## generate date list 
DATELIST=""
## monthly chunks 
YM=$START_MON
while [ $YM -le $END_MON ];
do
    DATELIST="$DATELIST $YM"
    YM=$(date -u -d "${YM}01 + 1 month" +"%Y%m")
done
#Daily chunks 
YMD=$START_DAY
while [ $YMD -le $END_DAY ];
do
    DATELIST="$DATELIST $YMD"
    YMD=$(date -u -d "${YMD} +1 day" +"%Y%m%d")
done


## Loop on date chunks 
for YM in $DATELIST
do
    echo $YM
    ## gridded analysis 
    ./extract_mars_ga.sh --class $class --expver $expver --efreq $efreq \
                         --edate $YM --draw $draw --dstats $dstats
    ./compute_stats.sh --class $class --expver $expver --efreq $efreq \
                       --edate $YM --draw $draw --dstats $dstats --dtype ga
    ## gridded satellite data 
    ./extract_mars_gsd.sh --class $class --expver $expver --efreq $efreq \
                         --edate $YM --draw $draw --dstats $dstats --gsdvars $gsdvars 
    ./compute_stats.sh --class $class --expver $expver --efreq $efreq \
                       --edate $YM --draw $draw --dstats $dstats --dtype gsd

done
exit 0 
