## Script to plot the CAMS monitoring 
## E. Dutra IPMA, April 2024
##    Not very well organized/generic could be improved... 


import xarray as xr
import numpy as np
import matplotlib.pyplot as plt
from datetime import datetime, timedelta
import pandas as pd 
import glob
import matplotlib.dates as mdates
import matplotlib as mplt


def load_data(base_dir,eclass,expver,efreq,domain,ftype):
    
    # files path for glob 
    fpath=f"{base_dir}/gfas_{ftype}_{eclass}_{expver}_????_{efreq}_{domain}.nc"
    files = glob.glob(fpath)
    # load files 
    dd=[]
    for ff in sorted(files):
        dd.append(xr.open_dataset(ff))
    ds = xr.concat(dd,dim='time')
    
    ## check temporal consistency
    #if efreq == '1d': efreq_='1D'
    #dd_tmp = pd.date_range(start=ds.time[0].values,end=ds.time[-1].values,freq=efreq_,inclusive='both')
    #if ds.time.size != dd_tmp.size:
        #print(f"Loaded temporal series has some gaps. Missing {dd_tmp.size-ds.time.size} slots" )
        #for kk in dd_tmp:
            #if kk not in ds.time:
                #print(kk)
    return ds 

def config_axis(ax):
    ax.grid(linestyle=':')
    ax.tick_params(axis='x', labelrotation = 45)
    ax.xaxis.set_minor_locator(mdates.DayLocator(interval=1))
    ax.xaxis.set_major_locator(mdates.DayLocator(interval=5))
    ax.xaxis.set_major_formatter(mdates.DateFormatter('%m-%d'))
    

def get_args():
    from argparse import ArgumentParser,ArgumentDefaultsHelpFormatter

    description = 'Plot GFAS statistics'
    parser = ArgumentParser(description=description,formatter_class=ArgumentDefaultsHelpFormatter)
    parser.add_argument('--ptype',dest='ptype',type=str,default='NONE',
                     help="plot type: GA, GSD, GSD_C")
    parser.add_argument('--dstats',dest='base_dir',type=str,default='NONE',
                     help="base directorty with input files")
    parser.add_argument('--domain',dest='domain',type=str,default='NONE',
                     help="Domain")
    parser.add_argument('--expver',dest='expver',type=str,default='NONE',
                     help="experiment version")
    parser.add_argument('--edate',dest='lastdate',type=str,default='NONE',
                     help="lastdate to plot YYYYMMDD")
    parser.add_argument('--class',dest='eclass',type=str,default='NONE',
                     help="data class")
    parser.add_argument('--efreq',dest='efreq',type=str,default='NONE',
                     help="data frequency 1d / 1h ")
    parser.add_argument('--dplot',dest='savedir',type=str,default='NONE',
                     help="directory to save output ")
    parser.add_argument('--dformat',dest='dformat',type=str,default='NONE',
                     help="plot output format: png / pdf ")
    opts = parser.parse_args()
    return opts  

mplt.rc('xtick', labelsize=9)
mplt.rc('ytick', labelsize=9)
plot_window= 35 #365  # number of day to plot 

# get command line arguments
opts = get_args()

dend = datetime.strptime(opts.lastdate,"%Y%m%d")
dstart = dend - timedelta(days=plot_window-1)


if opts.ptype == "GSD_C" :
    ### Plot compare different sensore for the period 
    
    cvars={'MODISTerra':'783_389',
           'MODISAqua':'784_389',
           'VIIRSNPP':'224_616',
           }
    colors={'MODISTerra':'b',
            'MODISAqua':'r',
            'VIIRSNPP':'g',
               }
    
    ds = load_data(opts.base_dir,opts.eclass,opts.expver,
                   opts.efreq,opts.domain,'gsd')
    
        
    divFac=1e9  
    ## actual date
    ds_tmp = ds.sel(time=slice(dstart,dend))
    
    fig,axx=plt.subplots(2,1,figsize=(6,7))
    ## loop on frpfire and offire 
    for ikx,vtype in enumerate(['frpfire','offire']):
        # create plot 
        divFac=1e9
        if vtype=='offire' and opts.expver != "0001":
            divFac=1e12
        ax=axx[ikx]
   
        ## loop on instruments 
        for cinst in cvars.keys():
            cvar=f"{vtype}_{cvars[cinst]}"
            try:
                # in a try as not all instruments might be available 
                ax.plot(ds_tmp.time.values,ds_tmp[cvar].values.squeeze()/divFac,
                        lw=1.5,c=colors[cinst],label=cinst)
            except:
                pass 
        ax.set_title(f"Gridded satellite {vtype} {opts.domain} {opts.eclass}-{opts.expver} {opts.efreq} ",size=9)
        if vtype=='frpfire': ax.set_ylabel('FRP (GW)',fontsize=9)
        
        if vtype=='offire': 
            if opts.expver == "0001":
                ax.set_ylabel('Area (1e9 m$^2$))',fontsize=9)
            else:
                ax.set_ylabel('Area sum Inv FRP variance (1e12 $m^6W^{-2}$))',fontsize=9)
                
        ax.set_xlim(dstart,dend)
        ax.legend(prop={'size':9})
        config_axis(ax) 
    plt.tight_layout()
    fout=f"{opts.savedir}/gfas_gsd_c_{opts.eclass}_{opts.expver}_{opts.lastdate}_{opts.efreq}_{opts.domain}.{opts.dformat}"
    print("Saving to:",fout)
    plt.savefig(fout)
    #plt.show()
    
if opts.ptype == "GSD" :
    ### Plot FRP GSD for each satellite observation, comparing with previous 5 years
    ds = load_data(opts.base_dir,opts.eclass,opts.expver,
                   opts.efreq,opts.domain,'gsd')
    
    for cv,cname in (("783_389",'MODISTerra'),("784_389",'MODISAqua'),('224_616','VIIRSNPP')):
        cvar=f"frpfire_{cv}"

        if  cvar not in list(ds.keys()) :
            # skip if variable not found 
            continue
    
        ## create plot 
        fig,ax=plt.subplots(1,1,figsize=(6,3.5))
        
        ## actual date
        ds_tmp = ds.sel(time=slice(dstart,dend))
        
        ## go back in time 
        ikY=1
        while True:
            dstart_=dstart - timedelta(days=365*ikY)
            dend_=dend - timedelta(days=365*ikY)
            ds_tmp_ = ds.sel(time=slice(dstart_,dend_))
            if ds_tmp_.time.size == 0 : break
            lgd = "hist"
            if ikY>1:lgd=None
            xtime = ds_tmp_.time.values + np.timedelta64(365*ikY,'D')
            ax.plot(xtime,ds_tmp_[cvar].values.squeeze()/1e9,lw=0.5,c='gray',label=lgd)
            ikY = ikY + 1
            
        ax.plot(ds_tmp.time.values,ds_tmp[cvar].values.squeeze()/1e9,lw=1.5,c='darkorange',label=str(dend.year))
        ax.set_title(f"Gridded FRP {cname} {opts.domain} {opts.eclass}-{opts.expver} {opts.efreq} (hist={ikY-1} yrs)",size=9)
        ax.set_ylabel('FRP (GW)',fontsize=9)
        ax.set_xlim(dstart,dend)
        ax.legend(prop={'size':9})
        config_axis(ax) 
        plt.tight_layout()
        fout=f"{opts.savedir}/gfas_gsd_{cname}_{opts.eclass}_{opts.expver}_{opts.lastdate}_{opts.efreq}_{opts.domain}.{opts.dformat}"
        print("Saving to:",fout)
        plt.savefig(fout)
        #plt.show()

if opts.ptype == "GA" :
    ### Plot FRP GA comparing with previous 5 years
    ds = load_data(opts.base_dir,opts.eclass,opts.expver,
                   opts.efreq,opts.domain,'ga')
    
    ## create plot 
    fig,ax=plt.subplots(1,1,figsize=(6,3.5))
    
    ## actual date
    ds_tmp = ds.sel(time=slice(dstart,dend))
    
    ## go back in time 
    ikY=1
    while True:
        dstart_=dstart - timedelta(days=365*ikY)
        dend_=dend - timedelta(days=365*ikY)
        ds_tmp_ = ds.sel(time=slice(dstart_,dend_))
        #print(dstart_,dend_,ds_tmp_.time.size)
        if ds_tmp_.time.size == 0 : break
        lgd = "hist"
        if ikY>1:lgd=None
        xtime = ds_tmp_.time.values + np.timedelta64(365*ikY,'D')
        ax.plot(xtime,ds_tmp_['frpfire'].values.squeeze()/1e9,lw=0.5,c='gray',label=lgd)
        ikY = ikY + 1 
        
    ax.plot(ds_tmp.time.values,ds_tmp['frpfire'].values.squeeze()/1e9,lw=1.5,c='darkorange',label=str(dend.year))
    ax.set_title(f"GFAS FRP {opts.domain} {opts.eclass}-{opts.expver} {opts.efreq} (hist={ikY-1} yrs)",size=9)
    ax.set_ylabel('FRP (GW)',fontsize=9)
    ax.set_xlim(dstart,dend)
    ax.legend(prop={'size':9})
    config_axis(ax) 
    plt.tight_layout()
    fout=f"{opts.savedir}/gfas_ga_{opts.eclass}_{opts.expver}_{opts.lastdate}_{opts.efreq}_{opts.domain}.{opts.dformat}"
    print("Saving to:",fout)
    plt.savefig(fout)
    #plt.show()
