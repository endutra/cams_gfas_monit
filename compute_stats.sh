#!/bin/bash 

## script to compute statistics (using cdo)
## E. Dutra, IPMA April 2024 
 

module load cdo

print_help() {
	echo "Usage: "compute_stats.sh" <args>" >&2
	exit 0
}
opts=$(getopt -o '' --long class:,expver:,edate:,efreq:,draw:,dstats:,dtype: -- "$@") || print_help
eval set -- "$opts"
while (($#))
do
  case $1 in
    --class)   class=$2; shift;;
    --expver)  expver=$2; shift;;
    --edate)   edate=$2; shift;;
    --efreq)   efreq=$2; shift;;
    --draw)    draw=$2; shift;;
    --dstats)  dstats=$2; shift;;
    --dtype)   dtype=$2;  shift;;
    --)        shift; break;;
    esac
    shift 
done


set -eu

## Get region definitions 
. ./areas_definition.sh 
 
start=$(date +'%s') 
echo "Starting compute_stats  $dtype $class $expver $edate $efreq $draw $dstats"


# for cdo: If set to 1, skips all consecutive timesteps with a double entry of the same timestamp.
export SKIP_SAME_TIME=1 

# Create draw output directory 
mkdir -p $draw
mkdir -p $dstats


# year we're processing 
year=${edate:0:4}


ftag=${dtype}_${class}_${expver}_${edate}_${efreq} # file name "tag"
ftagY=${dtype}_${class}_${expver}_${year}_${efreq} # file name "tag" for year stats 
#Generate input file path 
finput=${draw}/gfas_${ftag}.nc 


if [ ! -r $finput ]; then
    echo "Expected input file not present !"
    exit 0
fi 


# check if gridarea exists, otherwise generate it 
if [ ! -r ${dstats}/gridarea.nc ]; then
    cdo gridarea $finput ${dstats}/gridarea.nc
fi 

## multiply by area before to speed it up 
cdo -s -f nc4 -z zip_1 mul $finput  ${dstats}/gridarea.nc ${finput}_GAREA

## Loop in domains 
ndomains=${#DOMAINS[@]} 
for (( i=0; i<$ndomains; i++));
do
    echo "Processing ${DOMAINS[i]} ${DOMAINSL[i]}"
    
    # do the fieldsum over the selected area 
    fout=${dstats}/gfas_${ftag}_${DOMAINS[i]}.nc 
    cdo -s -fldsum -sellonlatbox,${DOMAINSL[i]} ${finput}_GAREA $fout
    
    ## concatenate in yearlly file
    foutY=${dstats}/gfas_${ftagY}_${DOMAINS[i]}.nc 
    if [ -r $foutY ]; then
        cdo -Q -s mergetime $foutY $fout ${foutY}_NEW
        mv ${foutY}_NEW $foutY
    else
        mv $fout ${foutY}
    fi
    
    rm -f $fout 
done 
rm -f ${finput}_GAREA

echo "Done compute_stats ${dtype}_${expver}_${edate} in  $(($(date +'%s') - $start)) seconds"


