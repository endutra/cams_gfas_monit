#!/bin/bash 

## script to extract GFAS analysis 
## E. Dutra, IPMA April 2024 

## extract_mars_gfas.sh ---

module load ecmwf-toolbox
module load cdo 

set -eu

print_help() {
	echo "Usage: "extract_mars_ga.sh" <args>" >&2
	exit 0
}
opts=$(getopt -o '' --long class:,expver:,edate:,efreq:,draw:,dstats: -- "$@") || print_help
eval set -- "$opts"
while (($#))
do
  case $1 in
    --class)   class=$2; shift;;
    --expver)  expver=$2; shift;;
    --edate)   edate=$2; shift;;
    --efreq)   efreq=$2; shift;;
    --draw)    draw=$2; shift;;
    --dstats)  dstats=$2; shift;;
    --)           shift; break;;
    esac
    shift 
done

# set -x
 
start=$(date +'%s') 
echo "Starting extract_mars_ga.sh $class $expver $edate $efreq $draw $dstats"


# Create draw output directory 
mkdir -p $draw
mkdir -p $dstats

## Prepare MARS retrieval 

## daily data
if [ $efreq == "1d" ]; then
    step="0-24"
    time=0
## hourly data
elif [ $efreq == "1h" ]; then
    step="0-1"
    time="0/to/23/by/1"
fi

## case of monthly retrieval
if [ ${#edate} -eq 6 ] ; then
    ldate=$(date -u -d "${edate}01 +1 month -1 day" +"%Y%m%d")
    ddate="${edate}01/to/${ldate}"
else
    ddate=$edate
fi
    
ftag=${class}_${expver}_${edate}_${efreq} # file name "tag"
fout=${draw}/gfas_ga_${ftag}  # output file
set +e 
mars << EOF
retrieve,
class=$class,
date=$ddate,
expver=$expver,
levtype=sfc,
param=99.210,
step=$step,
stream=gfas,
time=$time,
type=ga,
target="${fout}.grb"
EOF
set -e 

## interpolate to regular 1x1 conservative and netcdf
if [ ! -r ${dstats}/remapw.nc ]; then
    # generate remap weights 
    cdo --eccodes -P 2 -f nc4 -z zip_1 gencon,global_1 ${fout}.grb ${dstats}/remapw.nc
fi 
cdo --eccodes -f nc4 -z zip_1 remap,global_1,${dstats}/remapw.nc ${fout}.grb ${fout}.nc
rm -f ${fout}.grb

echo "Done extract_mars_ga ${ftag} in  $(($(date +'%s') - $start)) seconds"
